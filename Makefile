SHELL=/bin/bash

all: ssh-all 

ssh-all: ssh-pull ssh-update

ssh-pull:
	@git pull -r  1>/dev/null 2>&1

ssh-update: 
	@./conf/update
